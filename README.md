# cripapp (React Native)

## Prerrequisites

- NodeJS
- Java JDK 11
- Android Studio
- Android SDK
- Android SDK Tools:
    1. Build-tools
    2. Command-line-tools
    3. Emulator
    4. Emulator Hypervisor AMD o Emulator Hypervisor Intel (depende de tu procesador)
    5. Platform-tools
    6. USB Driver
- Crear AVD en Android Studio
- Aniadir `Android/Sdk` al PATH de windows como una nueva variable llamada "ANDROID_HOME"
- Aniadir `%LOCALAPPDATA%\Android\Sdk\platform-tools` al la variable PATH
- Aniadir `%LOCALAPPDATA%\Android\Sdk\tools\bin` al la variable PATH
- Aniadir `%LOCALAPPDATA%\Android\Sdk\tools` al la variable PATH
- Aniadir `%LOCALAPPDATA%\Android\Sdk\emulator` al la variable PATH

## Execute

### Android

```BASH
npm i --force
```

```BASH
npx react-native run-android
```

### iOS

```BASH
npm i --force
```

```BASH
cd ios && pod install
```

```BASH
npx react-native run-ios
```

## Deploy

### Android

- Dentro de `android/app/src/main/` crear la carpeta: `assets`

- Abre la terminal desde la ruta de tu proyecto y ejecuta los comandos:

    ```BASH
    npm i --force
    ```

    ```BASH
    npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
    ```

    ```BASH
    cd android && gradlew assembleDebug
    ```

- Listo ya tienes tu APK generada y la puedes encontrar en el siguiente directorio de tu proyecto: `project/android/app/build/outputs/apk/debug/app-debug.apk`

### iOS

- Abre la terminal desde la ruta de tu proyecto y ejecuta los comandos:

    ```BASH
    npx react-native run-ios --configuration=release
    ```
